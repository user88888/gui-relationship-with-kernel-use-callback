﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

public class kernel
{
    private sign_in_window _sign_in_win;
    private Thread _sign_win_thread;
    private Thread console_thread;

    private board_window _board_number_win;
    private Thread _board_number_thread;

    private manual_control_window _manual_control_win;
    private Thread _manual_control_thread;

    // <string, bool>
    // string - window name
    // bool - true: init and run, false: null
    private Dictionary<string, bool> wins_set = new Dictionary<string, bool>()
    {
        { "sign_in", false },
        { "board_number", false },
        { "manual_control", false}
    };

    private string _sign_in = "sign_in";
    private string _board_number = "board_number";
    private string _manual_control = "manual_control";

    public kernel()
    {

    }

    public void init_win()
    {
        this._sign_in_win = new sign_in_window();
        this._sign_in_win.init();
        this._sign_in_win.init_ok_button_callback(() => {
            Console.WriteLine("login " + this._sign_in_win.getData().Item1 +
                "\npassword " + this._sign_in_win.getData().Item2);
        });

        this._sign_win_thread = new Thread(() => { this._sign_in_win.run(); });
        this._sign_win_thread.Start();
    }

    public void commands_list()
    {
        Console.WriteLine(
            "Commands list\n" +
            "1. [create sign_in]\n" +
            "2. [create board_number]\n" +
            "3. [create manual_control]\n" +
            "4. [dispose sign_in]\n" +
            "5. [dispose board_number]\n" +
            "6. [dispose manual_control]\n"
            );
    }

    public void init()
    {
        this.console_thread = new Thread(() => {
            string str = "";
            do
            {
                Console.Write("Enter command> \t");
                str = Console.ReadLine();
                if (str == "help" || str == "-h")
                {
                    this.commands_list();
                }
                else if (str.Contains("create"))
                {
                    if(str.Contains(_sign_in))
                    {
                        if(!this.wins_set[_sign_in])
                        {
                            this.create_sign_in_win();
                        }
                        else
                        {
                            Console.WriteLine("\n[ ERROR ] Can't create sign_in window. Dispose it before create.");
                        }
                    }
                    else if(str.Contains(_board_number))
                    {
                        if (!this.wins_set[_board_number])
                        {
                            this.create_board_number_win();
                        }
                        else
                        {
                            Console.WriteLine("\n[ ERROR ] Can't create board_number window. Dispose it before create.");
                        }
                    }
                    else if(str.Contains(_manual_control))
                    {
                        if (!this.wins_set[_manual_control])
                        {
                            this.create_manual_control_win();
                        }
                        else
                        {
                            Console.WriteLine("\n[ ERROR ] Can't create manual_control window. Dispose it before create.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("[ ERROR ] wind not found");
                    }
                }
                else if (str.Contains("dispose"))
                {

                }
                

            } while (str != "end");
        });

        this.console_thread.Start();
    }

    public void create_sign_in_win()
    {
        this._sign_in_win = new sign_in_window();
        this._sign_in_win.init();
        this._sign_win_thread = new Thread(this._sign_in_win.run);
        this._sign_win_thread.Start();
        this.wins_set[_sign_in] = true;
    }

    public void create_board_number_win()
    {
        this._board_number_win = new board_window();
        this._board_number_win.init();
        this._board_number_thread = new Thread(this._board_number_win.run);
        this._board_number_thread.Start();
        this.wins_set[_board_number] = true;
    }

    public void create_manual_control_win()
    {
        this._manual_control_win = new manual_control_window();
        this._manual_control_win.init();
        this._manual_control_thread = new Thread(this._manual_control_win.run);
        this._manual_control_thread.Start();
        this.wins_set[_manual_control] = true;
    }

    public void dispose_sign_in_win()
    {
        if(this._sign_in_win != null)
        {
            this._sign_in_win.dispose();
            this.wins_set[_sign_in] = false;
        }
        else
        {
            Console.WriteLine("[ ERROR ] can't dispose sign in window. Sign in window was't create.");
        }
    }

    public void dispose_board_number_win()
    {
        if (this._board_number_win != null)
        {
            this._board_number_win.dispose();
            this.wins_set[_board_number] = false;
        }
        else
        {
            Console.WriteLine("[ ERROR ] can't dispose sign in window. Sign in window was't create.");
        }
    }

    public void dispose_manual_control_win()
    {
        if (this._manual_control_win != null)
        {
            this._manual_control_win.dispose();
            this.wins_set[_manual_control] = false;
        }
        else
        {
            Console.WriteLine("[ ERROR ] can't dispose sign in window. Sign in window was't create.");
        }
    }
}
