﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;


public class sign_in_window : win_base
{
    private TextBox login_text_box;
    private TextBox password_text_box;

    private Label login_label;
    private Label password_label;

    private Button ok_button;

    private Form sign_in_win_form;

    private static int WIN_X = 440;
    private static int WIN_Y = 280;

    private Size win_size = new Size(WIN_X, WIN_Y);
    private Size content_size = new Size(200, 20);

    private string sign_in_text = "SIGN IN";
    private string login_label_text = "login";
    private string password_label_text = "password";
    private string ok_button_text = "OK";

    private Action ok_button_callback;

    public sign_in_window()
    {

    }

    public void init()
    {
        this.sign_in_win_form = new Form();
        this.sign_in_win_form.Size = win_size;
        this.sign_in_win_form.Text = sign_in_text;
        this.sign_in_win_form.ShowIcon = false;
        this.sign_in_win_form.FormBorderStyle = FormBorderStyle.FixedSingle;

        int locX, locY, locLX, nextY;
        nextY = 25;
        locX = sign_in_win_form.Width / 2 - content_size.Width / 2;
        locY = sign_in_win_form.Height / 2 - (content_size.Height * 3 + nextY * 3) / 2;
        locLX = locX + content_size.Width + 25;

        // LOGIN ---------------------------------------------------------

        this.login_text_box = new TextBox();
        this.login_text_box.Size = this.content_size;
        this.login_text_box.Location = new Point(locX, locY);

        this.login_label = new Label();
        this.login_label.Size = this.content_size;
        this.login_label.Location = new Point(locLX, locY);
        this.login_label.Text = this.login_label_text;

        // ---------------------------------------------------------------

        locY += nextY;

        // PASSWORD ------------------------------------------------------

        this.password_text_box = new TextBox();
        this.password_text_box.Size = this.content_size;
        this.password_text_box.Location = new Point(locX, locY);
        this.password_text_box.PasswordChar = '*';

        this.password_label = new Label();
        this.password_label.Size = this.content_size;
        this.password_label.Location = new Point(locLX, locY);
        this.password_label.Text = this.password_label_text;

        // ---------------------------------------------------------------

        locY += nextY;

        // OK ------------------------------------------------------------

        this.ok_button = new Button();
        this.ok_button.Size = this.content_size;
        this.ok_button.Location = new Point(locX, locY);
        this.ok_button.Text = ok_button_text;
        this.ok_button.Click += new EventHandler(event_ok_button_pressed);

        // ---------------------------------------------------------------

        this.sign_in_win_form.Controls.Add(this.login_text_box);
        this.sign_in_win_form.Controls.Add(this.login_label);
        this.sign_in_win_form.Controls.Add(this.password_text_box);
        this.sign_in_win_form.Controls.Add(this.password_label);
        this.sign_in_win_form.Controls.Add(this.ok_button);

    }

    public void run()
    {
        Application.Run(this.sign_in_win_form);
    }

    public Tuple<string, string> getData()
    {
        return (login_text_box == null || password_text_box == null
                || login_text_box.Text.Length == 0 || password_text_box.Text.Length == 0) ?
            null : new Tuple<string, string>(login_text_box.Text, password_text_box.Text);
    }

    private void event_ok_button_pressed(object sender, EventArgs args)
    {
        ok_button_callback();
    }

    public void init_ok_button_callback(Action callback)
    {
        ok_button_callback = callback;
    }

    public void dispose()
    {
        this.sign_in_win_form.Dispose();
    }
}

