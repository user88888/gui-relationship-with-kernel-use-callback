﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

public class board_window : win_base
{
    private TextBox board_number_text_box;

    private Label board_number_label;

    private Button ok_button;

    private Form board_win_form;

    private static int WIN_X = 440;
    private static int WIN_Y = 280;

    private Size win_size = new Size(WIN_X, WIN_Y);
    private Size content_size = new Size(200, 20);

    private string board_number_window_text = "BOARD NUMBER";
    private string board_number_label_text = "number";
    private string ok_button_text = "OK";

    private Action ok_button_callback;

    public board_window()
    {

    }

    public void init()
    {
        this.board_win_form = new Form();
        this.board_win_form.Size = win_size;
        this.board_win_form.Text = board_number_window_text;
        this.board_win_form.ShowIcon = false;
        this.board_win_form.FormBorderStyle = FormBorderStyle.FixedSingle;

        int locX, locY, locLX, nextY;
        nextY = 25;
        locX = board_win_form.Width / 2 - content_size.Width / 2;
        locY = board_win_form.Height / 2 - (content_size.Height * 2 + nextY * 2) / 2;
        locLX = locX + content_size.Width + 25;

        // LOGIN ---------------------------------------------------------

        this.board_number_text_box = new TextBox();
        this.board_number_text_box.Size = this.content_size;
        this.board_number_text_box.Location = new Point(locX, locY);

        this.board_number_label = new Label();
        this.board_number_label.Size = this.content_size;
        this.board_number_label.Location = new Point(locLX, locY);
        this.board_number_label.Text = this.board_number_label_text;

        // ---------------------------------------------------------------

        locY += nextY;

        // OK ------------------------------------------------------------

        this.ok_button = new Button();
        this.ok_button.Size = this.content_size;
        this.ok_button.Location = new Point(locX, locY);
        this.ok_button.Text = ok_button_text;
        this.ok_button.Click += new EventHandler(_event_ok_button_pressed);

        // ---------------------------------------------------------------

        this.board_win_form.Controls.Add(this.board_number_text_box);
        this.board_win_form.Controls.Add(this.board_number_label);
        this.board_win_form.Controls.Add(this.ok_button);
    }

    public void run()
    {
        Application.Run(this.board_win_form);
    }

    public void dispose()
    {
        this.board_win_form.Dispose();
    }

    private void _event_ok_button_pressed(object sender, EventArgs args)
    {
        this.ok_button_callback();
    }

    public void init_ok_button_pressed_callback(Action callback)
    {
        this.ok_button_callback = callback;
    }

    public string board_number()
    {
        return (this.board_number_text_box.Text.Length != 0) ?
            this.board_number_text_box.Text : null;
    }
}
