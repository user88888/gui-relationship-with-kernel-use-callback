﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;


public class manual_control_window : win_base
{
    private Form manual_control_window_form;
    private Label manual_control_label;
    private Button manual_control_button_ok;
    private DataGridView table;

    private static int WIN_X = 640;
    private static int WIN_Y = 480;

    private Size win_size = new Size(WIN_X, WIN_Y);
    private Size content_size = new Size(200, 20);

    private string manual_control_window_text = "MANUAL CONTROL";
    private string manual_control_label_text = "manual control";
    private string manual_control_button_ok_text = "OK";

    private Dictionary<string, string> data = new Dictionary<string, string>();

    private Action ok_button_callback;

    public manual_control_window()
    {

    }

    public void init()
    {
        this.manual_control_window_form = new Form();
        this.manual_control_window_form.Size = win_size;
        this.manual_control_window_form.Text = manual_control_window_text;
        this.manual_control_window_form.ShowIcon = false;
        this.manual_control_window_form.FormBorderStyle = FormBorderStyle.FixedSingle;

        int locX, locY, locLX, nextY;
        nextY = 25;
        locX = 10;
        locY = 10;
        locLX = locX;
        int loc_button_y = this.manual_control_window_form.Height - 100;

        this.manual_control_label = new Label();
        this.manual_control_label.Size = this.content_size;
        this.manual_control_label.Location = new Point(locLX, locY);
        this.manual_control_label.Text = this.manual_control_label_text;

        locY += nextY;

        this.table = new DataGridView();
        this.table.Size = new Size(300, 330);
        this.table.Location = new Point(locX, locY);
        this.table.Columns.Add("param", "Параметр");
        this.table.Columns.Add("value", "Значение");

        this.manual_control_button_ok = new Button();
        this.manual_control_button_ok.Size = this.content_size;
        this.manual_control_button_ok.Location = new Point(locX, loc_button_y);
        this.manual_control_button_ok.Text = manual_control_button_ok_text;
        this.manual_control_button_ok.Click += new EventHandler(_event_manual_control_button_ok_pressed);

        this.manual_control_window_form.Controls.Add(this.manual_control_label);
        this.manual_control_window_form.Controls.Add(this.manual_control_button_ok);
        this.manual_control_window_form.Controls.Add(this.table);
    }

    public void run()
    {
        Application.Run(this.manual_control_window_form);
    }

    public void dispose()
    {
        this.manual_control_window_form.Dispose();
    }

    private void _event_manual_control_button_ok_pressed(object sender, EventArgs args)
    {
        this.ok_button_callback();
    }

    public void init_ok_button_pressed_callback(Action callback)
    {
        this.ok_button_callback = callback;
    }

    public Dictionary<string, string> get_data() {
        return (this.data.Count != 0) ? new Dictionary<string, string>(this.data) : null;
    }
}

